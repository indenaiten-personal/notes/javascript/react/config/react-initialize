# Configuración Inicial de un proyecto con React

> **Autor:** _Ángel Herce Soto_  
> **Fecha:** _27/12/2021_

## Pasos para crear el proyecto:

- Instalamos [NodeJs](https://nodejs.dev/download/).

- Abrimos una terminal en la raíz de la carpeta del proyecto.

- Ejecutamos en la terminal el siguiente comando para inicializar el proyecto con `npm`:
  ```
  npm init
  ```
  
- Instalamos los paquetes `react` y `react-dom` ejecutando en la terminal el siguiente comando para poder utilizar React:
  ```
  npm i --save-dev react react-dom
  ```
  
- Creamos en la raíz de la carpeta del proyecto las carpetas `src` y `public`.

- Instalamos los paquetes `webpack` y `webpack-cli` ejecutando en la terminal el siguiente comando para poder utilizar Webpack:
  ```
  npm i --save-dev webpack webpack-cli
  ```
  
- Creamos en la raíz de la carpeta del proyecto el fichero `webpack.config.js` con el siguiente contenido para establecer la configuración de Webpack:
  ```javascript
  const path = require( "path" );
  
  module.exports = {
      entry: "./src/index.js",
      output: {
          filename: "bundle.[hash].js",
          path: path.resolve( __dirname, "dist" )
      },
      mode: "production"
  }
  ```

- Añadimos en el la sección `scripts` del fichero `package.json` el siguiente script para poder compilar nuestros ficheros `.js`, `.css`, etc en base al fichero de configuración de `webpack.config.js`:
  ```json
  {...
    "scripts": {
      "build": "webpack"
    }
  ...}
  ```
  
- Instalamos los paquetes `@babel/core`, `@babel/preset-env` y `@babel/preset-react` ejecutando en la terminal el siguiente comando para poder utilizar babel y compilar nuestro JavaScript en versiones compatibles con los navegadores web y para poder compilar el `JSX` de React:
  ```
  npm i --save-dev @babel/core @babel/preset-env @babel/preset-react
  ```
  
- Instalamos los paquetes `babel-loader`, `style-loader`, `css-loader` y `sass-loader` ejecutando en la terminal el siguiente comando para que Webpack pueda procesar archivos de babel, css y sass:
  ```
  npm i --save-dev babel-loader style-loader css-loader sass-loader
  ```
  
- Instalamos el paquete de `node-sass` ejecutando en la terminal el siguiente comando para poder procesar los ficheros `.scss`:
  ```
  npm i --save-dev node-sass
  ```

- En el fichero `webpack.config.js` establecemos nuestros loaders de la siguiente manera:
  ```json
  {...
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/,
                  resolve: {
                      extensions: [".js", ".jsx"]
                  }
            },
            {
                test: /\.(css|scss)$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            }
        ]
    }
  ...}
  ```
  
- Creamos dentro de la raíz de la carpeta del proyecto el fichero `babel.config.json` con el siguiente contenido para establecer la configuración de Babel:
  ```json
  {
    "presets": ["@babel/preset-env", "@babel/preset-react"]
  }
  ```

- Dentro de la carpeta `public` creamos el fichero `index.html` con el siguiente contenido:
  ```html
  <!doctype html>
  <html lang="es">
      <head>
          <meta charset="UTF-8"/>
          <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
          <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
          <title>React Initialize</title>
      </head>
  
      <body>
          <div id="root"></div>
      </body>
  </html>
  ```
  
- Dentro de la carpeta `src` creamos el fichero `App.js` con el siguiente contenido:
  ```javascript
  import React, { Component } from "react";
  import "./App.css";
  import "./App.scss";
  
  export default class App extends Component{
      render(){
          return(
              <div className="app">
                  <h1>Hi World !</h1>
              </div>
          );
      }
  }
  ```

- También podemos crear dentro de la carpeta `src` el fichero `App.css` con el siguiente contenido para comprobar que los ficheros `.css` funcionan correctamente:
  ```css
  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  .app{
    width: 100vw;
    height: 100vh;
    background-color: black;
  }
  
  .app > h1{
    font-size: 5em;
  }
  ```

- También podemos crear dentro de la carpeta `src` el fichero `App.scss` con el siguiente contenido para comprobar que los ficheros `.scss` funcionan correctamente:
  ```scss
  $text-color: #FFF;
  
  .app{
    padding: auto{
      top: 200px;
    }
  
    > h1{
      color: $text-color;
      text-align: center;
      font-family: Arial;
    }
  }
  ```

- Dentro de la carpeta `src` creamos el fichero `index.js` con el siguiente contenido:
  ```javascript
  import React from "react";
  import ReactDOM from "react-dom";
  import App from "./App";
  
  ReactDOM.render( <App/>, document.getElementById( "root" ));
  ```

- Ejecutamos en la terminal el siguiente comando para comprobar que el proyecto compila correctamente con la configuración realizada:
  ```
  npm run build
  ```
  
- Por último nos faltaría establecer nuestro fichero `.js` generado con webpack en nuestro fichero `.html` que se encuentra en la carpeta `public`.
Para ello podríamos realizarlo a mano, pero vamos a instalar 2 plugins de webpack que se encargarán de eliminar el contenido de la carpeta `dist` (creada por Webpack al compilar nuestros archivos) y establecer nuestro fichero `.js` en el fichero `.html` de forma automática (cogerá el fichero `.html` de la carpeta `public` y creará una copia en en la carpeta `dist` con el fichero `.js` enlazado).  
Necesitamos instalar los paquetes `clean-webpack-plugin` y `html-webpack-plugin` ejecutando en la terminal el siguiente comando:
  ```
  npm i --save-dev clean-webpack-plugin html-webpack-plugin
  ```

- Para añadir los plugins que hemos instalado anteriormente, añadiremos el siguiente contenido al fichero `webpack.config.js`:
  ````javascript
  const { CleanWebpackPlugin } = require( "clean-webpack-plugin" );
  const HtmlWebpackPlugin = require( "html-webpack-plugin" );
  
  {...
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({ template: "./public/index.html", filename: "index.html", hash: true })
    ]
  ...}
  ````
  
- Para que las funcionalidades modernas de nuestros ficheros .js, .css, etc funcionen correctamente con navegadores antiguos instalaremos el paquete `core-js` ejecutando en la terminal el siguiente comando:
  ```
  npm i --save-dev core-js
  ```
  
  - A continuación reemplazaremos el contenido del fichero `babel.config.json` por el siguiente:
    ```json
    {
      "presets": [
        ["@babel/preset-env", { "corejs":  "3.20.1", "useBuiltIns": "usage" }],
        "@babel/preset-react"
      ]
    }
    ```

  - Finalmente añadiremos al fichero `package.json` el siguiente apartado:
    ```json
    {...
      "browserslist": "> 0.25%, not dead, not ie 11"
    ...}
    ```

- Por último podremos instalar el paquete `webpack-dev-server` ejecutando en la terminal el siguiente comando para poder compilar de forma automática nuestro proyecto cuando realizemos cambios en nuestros archivos:
  ```
  npm i -save-dev webpack-dev-server
  ```
  
  - A continuación en el fichero `webpack.config.js` cambiaremos el valor del campo `mode` por el de `development` y añadiremos el siguiente campo para poder debuggear en el navegador los ficheros `.js`:
    ```json
    {...
      devtool: "inline-source-map"
    ...}
    ```
  
  - Finalmente añadiremos el siguiente script a nuestro fichero `package.json`:
    ```json
    {...
      "build-watch": "webpack-dev-server --open"
    ...}
    ``` 