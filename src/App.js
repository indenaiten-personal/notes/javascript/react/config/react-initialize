import React, { Component } from "react";
import "./App.css";
import "./App.scss";

export default class App extends Component{
    render(){
        return(
            <div className="app">
                <h1>Hi World !</h1>
            </div>
        );
    }
}
